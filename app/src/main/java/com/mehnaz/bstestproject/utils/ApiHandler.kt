package com.mehnaz.bstestproject.utils

import androidx.annotation.Keep
import retrofit2.HttpException
import retrofit2.Response
import java.net.UnknownHostException

@Keep
 enum class Status {
    SUCCESS,
    ERROR, LOADING
}
@Keep
 data class ApiResponse<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val errorCode: Int? = null
) {
    companion object {
        fun <T> success(data: T): ApiResponse<T> =
            ApiResponse(status = Status.SUCCESS, data = data, message = null)

        fun <T> error(data: T?, message: String, errorCode: Int? = null): ApiResponse<T> =
            ApiResponse(
                status = Status.ERROR,
                data = data,
                message = message,
                errorCode = errorCode
            )
    }
}
 suspend fun <T> safeApiCall(responseFunction: suspend () -> T): ApiResponse<T> {
    return try {
        val response = responseFunction.invoke()
        ApiResponse.success(response)
    } catch (e: HttpException) {
        val errorCode = e.code()
        val errorMessage = e.message()
        ApiResponse.error(null, errorMessage ?: "An error occurred", errorCode)
    } catch (e: UnknownHostException) {
        ApiResponse.error(null, "Please check your internet connection")
    } catch (e: Exception) {
        ApiResponse.error(null, e.toString())
    }
}