package com.mehnaz.bstestproject.data.repositories

import com.mehnaz.bstestproject.data.ApiService
import com.mehnaz.bstestproject.utils.safeApiCall

class SearchlistRepository (private val apiService: ApiService) {
    suspend fun fetchRepositoryLists(query:String,sort:String) = safeApiCall {
        apiService.getRepositories(query,sort)
    }
}