package com.mehnaz.bstestproject.data

import com.mehnaz.bstestproject.data.remote.models.Item
import com.mehnaz.bstestproject.data.remote.models.RepositoryListModel

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("search/repositories")
    suspend fun getRepositories(@Query("q") query: String?,
                                @Query("sort") sort: String?): Call<MutableList<Item>>
}