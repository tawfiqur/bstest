package com.mehnaz.bstestproject.data.remote.models


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep

data class RepositoryListModel(
    @SerializedName("incomplete_results")
    val incompleteResults: Boolean,
    @SerializedName("items")
    val items: MutableList<Item>,
    @SerializedName("total_count")
    val totalCount: Int
)