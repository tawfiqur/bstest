package com.mehnaz.bstestproject.di

import android.content.Context
import com.mehnaz.bstestproject.data.ApiService
import com.mehnaz.bstestproject.data.repositories.SearchlistRepository
import com.mehnaz.bstestproject.ui.viewmodels.RepositoriesViewModelFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Module(val applicationContext: Context){
    val BASEURL ="https://api.github.com/"
    fun getBaseOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(
                PackageIdHeaderInterceptor()
            ).build()
    }
     private fun getRetrofitAPIInstance(): Retrofit {
         return Retrofit.Builder()
             .baseUrl(BASEURL)
             .addConverterFactory(GsonConverterFactory.create())
             .client(getBaseOkHttpClient())
             .build()
     }
    private fun getRepositoriesService(): ApiService {
        return getRetrofitAPIInstance().create(ApiService::class.java)
    }
    private val repositoryContent: SearchlistRepository = SearchlistRepository(getRepositoriesService())

    val factoryRepositoryVM: RepositoriesViewModelFactory
        get() = RepositoriesViewModelFactory(repositoryContent)
 }