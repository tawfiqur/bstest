package com.mehnaz.bstestproject.di

import okhttp3.Interceptor
import okhttp3.Response

class PackageIdHeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
                .addHeader("PackageId", "com.mehnaz.bstestproject")
                .build()
        )
    }
}