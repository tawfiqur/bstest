package com.mehnaz.bstestproject.di

import android.content.Context

object MyApp {

        private var _module: Module? = null

        fun module(context: Context): Module {

            return _module ?: synchronized(this) { _module ?: Module(context).also { _module = it } }
        }

        fun onDestroy() {
            _module = null
        }

}