package com.mehnaz.bstestproject.di

import androidx.fragment.app.Fragment

interface FragmentEntryPoint {
    val injector: Module
        get() = MyApp.module((this as Fragment).requireContext().applicationContext)
}