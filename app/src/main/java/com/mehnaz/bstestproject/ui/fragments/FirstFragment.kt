package com.mehnaz.bstestproject.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mehnaz.bstestproject.databinding.FragmentFirstBinding
import com.mehnaz.bstestproject.di.FragmentEntryPoint
import com.mehnaz.bstestproject.ui.adapters.RepositoryListItemAdapter
import com.mehnaz.bstestproject.ui.viewmodels.searchListViewModel
import com.mehnaz.bstestproject.utils.Status


class FirstFragment : Fragment() ,FragmentEntryPoint{

    private var _binding: FragmentFirstBinding? = null

    private lateinit var viewModel: searchListViewModel
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       setupViewModel()
        viewModel.fetchRepositories("android","stars")
        val adapter= RepositoryListItemAdapter()
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerview.layoutManager = layoutManager
        binding.recyclerview.adapter = adapter
        viewModel.items.observe(viewLifecycleOwner){it->
            if (it?.status==Status.SUCCESS){
                adapter.setData(it.data)
                Log.e("TAG","DATA: " + it.data)
            }

        }

    }
    private fun setupViewModel() {
        viewModel =
            ViewModelProvider(
                this,
                injector.factoryRepositoryVM
            )[searchListViewModel::class.java]
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}