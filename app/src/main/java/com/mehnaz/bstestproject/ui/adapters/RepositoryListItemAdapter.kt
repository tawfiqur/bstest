package com.mehnaz.bstestproject.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.mehnaz.bstestproject.R
import com.mehnaz.bstestproject.data.remote.models.Item

class RepositoryListItemAdapter() : RecyclerView.Adapter<RepositoryListItemAdapter.ViewHolder>() {
    private var data: List<Item> ?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false)
        return ViewHolder(v)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems()

         holder.itemView.setOnClickListener {

         }
    }


    override fun getItemCount(): Int {
        return data?.size!!
    }

    fun setData(datanew: List<Item>?) {
        this.data = datanew
         notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val context = itemView.getContext()
        fun bindItems() {
           val imageView:ImageView = itemView.findViewById(R.id.image)

        }
    }
}