package com.mehnaz.bstestproject.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mehnaz.bstestproject.data.remote.models.Item
import com.mehnaz.bstestproject.data.remote.models.RepositoryListModel

import com.mehnaz.bstestproject.data.repositories.SearchlistRepository
import com.mehnaz.bstestproject.utils.ApiResponse
import com.mehnaz.bstestproject.utils.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class searchListViewModel(private val searchlistRepository: SearchlistRepository) :
    ViewModel() {
    private val _items = MutableLiveData<MutableList<Item>>()
    val items: MutableLiveData<MutableList<Item>> get() = _items

    fun fetchRepositories(query:String,sort:String) = viewModelScope.launch(Dispatchers.IO) {
        val response = searchlistRepository.fetchRepositoryLists(query,sort)

        if (response.status == Status.SUCCESS) {
            val itemList = response.data


            _items.value = itemList?.items
        }


    }
}