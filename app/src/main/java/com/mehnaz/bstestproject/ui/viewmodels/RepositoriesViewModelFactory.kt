package com.mehnaz.bstestproject.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mehnaz.bstestproject.data.repositories.SearchlistRepository

class RepositoriesViewModelFactory (private val searchlistRepository: SearchlistRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return searchListViewModel(searchlistRepository) as T
    }
}